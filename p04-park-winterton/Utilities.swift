//
//  Utilities.swift
//  p04-park-winterton
//
//  Created by Seoyoon Park on 2/21/16.
//  Copyright © 2016 Gabrielle Winterton. All rights reserved.
//

import Foundation

extension Array {
    func randomElement() -> Element {
        let index = Int(arc4random_uniform(UInt32(self.count)))
        return self[index]
    }
}
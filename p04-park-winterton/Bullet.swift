//
//  Bullet.swift
//  p04-park-winterton
//
//  Created by Gabrielle Winterton on 2/17/16.
//  Copyright © 2016 Gabrielle Winterton. All rights reserved.
//

import UIKit
import SpriteKit

class Bullet: SKSpriteNode {

    init(imageName: String, bulletSound: String?) {
        let texture = SKTexture(imageNamed: imageName)
        super.init(texture: texture, color: SKColor.clearColor(), size: texture.size())
        if(bulletSound != nil){
            runAction(SKAction.playSoundFileNamed(bulletSound!, waitForCompletion: false))
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

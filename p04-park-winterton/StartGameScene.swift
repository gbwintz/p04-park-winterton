//
//  StartGameScene.swift
//  p04-park-winterton
//
//  Created by Gabrielle Winterton on 2/17/16.
//  Copyright © 2016 Gabrielle Winterton. All rights reserved.
//

import UIKit
import SpriteKit


class StartGameScene: SKScene {
    
    override func didMoveToView(view: SKView) {
        let startGameButton = SKSpriteNode(imageNamed: "newgamebtn")
        startGameButton.position = CGPointMake(size.width/2,size.height/2 - 100)
        startGameButton.name = "startgame"
        addChild(startGameButton)
        
        //backgroundColor = SKColor.blackColor()
        //let starField = SKEmitterNode(fileNamed: "StarField")
        //starField!.position = CGPointMake(size.width/2,size.height/2)
        //starField!.zPosition = -1000
        //addChild(starField!)
        
        backgroundColor = SKColor.blackColor()
        let invaderText = PulsatingText(fontNamed: "ChalkDuster")
        invaderText.setTextFontSizeAndPulsate("INVADERZ", theFontSize: 25)
        invaderText.position = CGPointMake(size.width/2,size.height/2 + 75)
        addChild(invaderText)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first
        let touchLocation = touch!.locationInNode(self)
        let touchedNode = self.nodeAtPoint(touchLocation)
        if(touchedNode.name == "startgame"){
            let gameOverScene = GameScene(size: size)
            gameOverScene.scaleMode = scaleMode
            let transitionType = SKTransition.flipHorizontalWithDuration(1.0)
            view?.presentScene(gameOverScene, transition: transitionType)
        }
    }
    
}
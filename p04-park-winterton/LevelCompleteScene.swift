//
//  LevelCompleteScene.swift
//  p04-park-winterton
//
//  Created by Seoyoon Park on 2/21/16.
//  Copyright © 2016 Gabrielle Winterton. All rights reserved.
//

import Foundation
import SpriteKit

class LevelCompleteScene:SKScene{
    
    override func didMoveToView(view: SKView) {
        self.backgroundColor = SKColor.blackColor()
        let startGameButton = SKSpriteNode(imageNamed: "nextlevelbtn")
        startGameButton.position = CGPointMake(size.width/2,size.height/2 - 100)
        startGameButton.name = "nextlevel"
        addChild(startGameButton)
        
        //backgroundColor = SKColor.blackColor()
        //let starField = SKEmitterNode(fileNamed: "StarField")
        //starField!.position = CGPointMake(size.width/2,size.height/2)
        //starField!.zPosition = -1000
        //addChild(starField!)
        
        self.backgroundColor = SKColor.blackColor()
        let invaderText = PulsatingText(fontNamed: "ChalkDuster")
        invaderText.setTextFontSizeAndPulsate("LEVEL COMPLETE", theFontSize: 15)
        invaderText.position = CGPointMake(size.width/2,size.height/2 + 75)
        addChild(invaderText)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first as UITouch!
        let touchLocation = touch.locationInNode(self)
        let touchedNode = self.nodeAtPoint(touchLocation)
        if(touchedNode.name == "nextlevel"){
            let gameOverScene = GameScene(size: size)
            gameOverScene.scaleMode = scaleMode
            let transitionType = SKTransition.flipHorizontalWithDuration(0.5)
            view?.presentScene(gameOverScene,transition: transitionType)            }
    }
}

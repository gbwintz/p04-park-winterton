//
//  PlayerBullet.swift
//  p04-park-winterton
//
//  Created by Seoyoon Park on 2/21/16.
//  Copyright © 2016 Gabrielle Winterton. All rights reserved.
//

import UIKit
import SpriteKit

class PlayerBullet: Bullet {
    
    
    override init(imageName: String, bulletSound:String?){
        super.init(imageName: imageName, bulletSound: bulletSound)
        self.physicsBody = SKPhysicsBody(texture: self.texture!, size: self.size)
        self.physicsBody?.dynamic = true
        self.physicsBody?.usesPreciseCollisionDetection = true
        self.physicsBody?.categoryBitMask = CollisionCategories.PlayerBullet
        self.physicsBody?.contactTestBitMask = CollisionCategories.Invader
        self.physicsBody?.collisionBitMask = 0x0
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
Contributors: Gabby Winterton and Seoyoon Park

Comments:
The Space Invaders project was completed by utilizing a tutorial found on https://maniacdev.com/2015/04/tutorial-make-a-space-invaders-game-with-swift-and-spritekit-with-physics-particles-and-more. The game implements a normal space invaders type game where the spaceship moves with the use of an accelerometer and fires with a touch. However, the simulator does not have the functionality to test the accelerometer.
